#![macro_use]

pub use std::fs;
use std::fmt::Display;
pub use std::io::Write;
use std::error::Error;
use ndarray::prelude::*;
pub use itertools;

#[cfg(test)]
mod tests {
    use super::write_iter;
    use super::*;
        #[test]
        fn my_macro() {
            let x = vec![1, 2, 3];
            let y = vec![4, 5, 6];
            write_iter!["data/path.dat", x, y];
                        }

}


/// Takes two arrays and saves them to a file. Panics if arrays are not of the same length
// IDEA: Implement as macro to take arbitrary nr of inputs
pub fn arrays_to_file<T, S>(filepath: &str, array1: &Array1<T>, array2: &Array1<S>) -> Result<(), Box<dyn Error>>
    where
        T: Display,
        S: Display
{
    assert_eq!(array1.len(), array2.len());
    let mut file_out = fs::File::create(&filepath)?;
    for (x, y) in array1.iter().zip(array2.iter()) {
        let line = format!("{}\t{}\n", x, y);
        file_out.write_all(line.as_bytes())?;
    }
    Ok(())
}

/// Takes any nr of types that can be coonverted to iterables and prints their
/// elements to the specified path.
/// WARNING: Is a very crude implementation. Does not check length of the iters
/// and conversion to lines is very crude as well.
#[macro_export(local_inner_macros)]
macro_rules! write_iter {
    ($filepath:expr, $( $x:expr ),* ) => {
        {
            use ::std::fs;
            use ::std::io::Write;

            let mut file_out = fs::File::create(&$filepath).expect("Could not create filepath");

            for a in $crate::itertools::izip!($($x.iter(), )*) {
                let line = std::format!("{:?}\n", a)
                    .replace(",", "\t")
                    .replace("(", "")
                    .replace(")", "");
                file_out
                    .write_all(line.as_bytes())
                    .expect("could not write");
            }

        }
    };
}


/// Reads in a whitespace separated file and converts it to an f64 array
pub fn read_to_array(filepath: &str) -> Result<Array2<f64>, Box<dyn Error>>
{
    let file_content = fs::read_to_string(filepath)?;
    let lines = file_content.split("\n");
    let lines: Vec<&str> = lines.filter(|&line| line != "").collect::<Vec<&str>>();

    let nr_rows = lines.len();
    let nr_cols: usize = {
        let line: Vec<&str> = lines[0].split_whitespace().collect();
        line.len()
    };

    let mut content: Vec<f64> = vec![0.0; nr_rows * nr_cols];

    for (i, line) in lines.iter().enumerate() {
        let line_content = line.split_whitespace();
        for (j, cont) in line_content.enumerate() {
            content[i * nr_cols + j] = cont.parse::<f64>()?;
        }
    }
    Ok(Array::from_shape_vec((nr_rows, nr_cols), content)?)
}
